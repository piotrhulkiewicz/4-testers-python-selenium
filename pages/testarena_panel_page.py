from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from utils.utils_methods import MyUtils

class PanelPage:

    def __init__(self, browser):
        self.browser = browser

    def panel_page_wait_for_load(self):
        wait = WebDriverWait(self.browser, 10)
        selector = (By.CSS_SELECTOR, '.j_msgResponse')
        response_button = wait.until(EC.element_to_be_clickable(selector))
        return response_button

    def send_random_message(self):
        message_input = self.browser.find_element(By.CSS_SELECTOR, '#j_msgContent')
        random_string = MyUtils.get_random_string(10)
        message_input.send_keys(random_string)
        return random_string
