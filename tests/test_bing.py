import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@pytest.fixture()
def browser():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # Otwarcie strony bing
    browser.get('https://bing.com')
    yield browser
    # zamknięcie przeglądarki
    browser.quit()


def test_searching_in_bing_with_reject_cookies(browser):
    # odrzucenie cookies
    wait = WebDriverWait(browser, 10)
    reject = (By.CSS_SELECTOR, '#bnp_btn_reject')
    wait.until(EC.presence_of_element_located(reject))
    reject_button = browser.find_element(By.CSS_SELECTOR, '#bnp_btn_reject')
    assert reject_button.is_displayed()
    reject_button.click()

    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, "#sb_form_q")

    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, "label#search_icon")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed() is True
    assert search_button.is_displayed() is True

    # Szukanie Vistula University
    search_input.send_keys('Vistula University')
    search_button.click()

    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, 'li.b_algo')
    assert len(search_results) > 2
    search_results_titles = browser.find_elements(By.CSS_SELECTOR, 'h2 > a')
    assert search_results_titles[0].text == 'Home - Vistula University'

