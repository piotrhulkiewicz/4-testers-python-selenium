
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')

    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, '#search_form_input_homepage')

    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.ID, 'search_button_homepage')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed()
    assert search_button.is_displayed()

    # Szukanie Vistula University
    search_input.send_keys('Vistula University')
    search_button.click()

    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, '.nrn-react-div')
    assert len(search_results) > 2

    # Sprawdzenie że jeden z tytułów to strona uczelni
    search_results_titles = browser.find_elements(By.CSS_SELECTOR, '[data-testid="result-title-a"]')
    assert search_results_titles[0].text == 'Home - Vistula University'
    # Zamknięcie przeglądarki
    browser.quit()

def test_searching_in_duckduckgo_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://bing.com')


    # Znalezienie paska wyszukiwania
    search_bar = browser.find_element(By.CSS_SELECTOR, "textarea#sb_form_q")


    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, "svg")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_bar.is_displayed() is True
    assert search_button.is_displayed() is True
