import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC
from pages.testarena_login_page import LoginPage
from pages.testarena_cockpit_page import CockpitPage
from pages.testarena_panel_page import PanelPage
import time


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # stworzrenie obiektu klasy (page object) LoginPage'a
    login_page = LoginPage(browser)
    # wywołanie metod na obiekcie klasy
    login_page.load()
    login_page.login_to_page(login_page.login, login_page.password)
    yield browser
    browser.quit()


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title="Wyloguj"]').is_displayed()


def test_chat_page_opened(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()
    assert browser.find_element(By.CSS_SELECTOR, '.article_left_box').is_displayed()


def test_messages_opened(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()
    panel_page = PanelPage(browser)
    response_button = panel_page.panel_page_wait_for_load()

    assert response_button.is_displayed()


def test_open_administration_panel(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    assert browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT').is_displayed()

def test_send_message(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()
    panel_page = PanelPage(browser)
    panel_page.panel_page_wait_for_load()
    random_message = panel_page.send_random_message()
    browser.find_element(By.CSS_SELECTOR, '.j_msgResponse').click()
    #time.sleep(0.1)
    wait = WebDriverWait(browser, 10)
    wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, '.message_content_text')))
    messages = browser.find_elements(By.CSS_SELECTOR, '#j_msgSingleThreadList li')
    assert random_message in messages[-1].text