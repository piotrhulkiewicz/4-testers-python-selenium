import time

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


# Przykładowy test zrobiony przez bota chatGPT
def test_example():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.google.com/")
    driver.set_window_size(1280, 720)
    # driver.maximize_window()
    assert 'Google' in driver.title
    time.sleep(2)
    driver.quit()


def test_example_bing():
    browser = webdriver.Chrome(ChromeDriverManager().install())
    browser.get("https://www.bing.com")
    assert 'Bing' in browser.title