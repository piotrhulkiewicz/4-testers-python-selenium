import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('http://demo.testarena.pl')
    yield browser
    browser.quit()


def test_add_and_search_new_project_on_demo_testarena(browser):
    # logowanie do strony testarena
    email_input = browser.find_element(By.CSS_SELECTOR, "#email")
    password_input = browser.find_element(By.CSS_SELECTOR, '#password')
    login_button = browser.find_element(By.CSS_SELECTOR, '#login')
    email_input.send_keys('administrator@testarena.pl')
    password_input.send_keys('sumXQQ72$L')
    login_button.click()
    # wyszukanie elementu administracyjnego
    administration_button = browser.find_element(By.CSS_SELECTOR, '.icon_tools.icon-20')
    administration_button.click()
    # dodanie projektu projektu
    add_project_button = browser.find_element(By.CSS_SELECTOR, '#content > article > nav > ul > li:nth-child(1) > a')
    add_project_button.click()
    input_project_name = browser.find_element(By.CSS_SELECTOR, '#name')
    input_project_describe = browser.find_element(By.CSS_SELECTOR, '#description')
    input_project_prefix = browser.find_element(By.CSS_SELECTOR, '#prefix')
    save_project_button = browser.find_element(By.CSS_SELECTOR, '#save')
    input_project_name.send_keys('4Testers_PH')
    input_project_prefix.send_keys('4Testers_PH')
    input_project_describe.send_keys('Projekt zaliczeniowy selenium')
    save_project_button.click()
    # wyszukanie projektu w bazie
    projects_button = browser.find_element(By.CSS_SELECTOR, '#wrapper > ul > li.item2 > a')
    projects_button.click()
    input_project_search = browser.find_element(By.CSS_SELECTOR, '#search')
    search_button = browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
    input_project_search.send_keys('4Testers_PH')
    search_button.click()
    assert browser.find_element(By.LINK_TEXT, '4Testers_PH').is_displayed()


