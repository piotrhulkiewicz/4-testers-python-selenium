import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support import expected_conditions


@pytest.fixture()
def browser():
    # FAZA 1 - wykona się przed każdym testem
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # Otwarcie strony awesome-testing.com
    browser.get('https://www.awesome-testing.com')
    # FAZA 2 - coś co przekazujemy do każdego testu
    yield browser
    # Faza 3 - wykona się po każdym teście
    # Zamknięcie przeglądarki
    browser.quit()


def test_post_count(browser):

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-outer')

    # Asercja że lista ma 4 elementy
    assert len(posts) == 4


def test_post_count_after_search(browser):

    # Inicjalizacja searchbara i przycisku search
    input_search = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    button_search = browser.find_element(By.CSS_SELECTOR, 'td input.gsc-search-button')
    # Szukanie
    input_search.send_keys('cypress')
    button_search.click()

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CLASS_NAME, 'post-title')
    # Asercja że lista ma 3 elementy
    assert len(titles) == 6


def test_post_count_on_cypress_label(browser):

    # Inicjalizacja elementu z labelką
    cypres_label = browser.find_element(By.LINK_TEXT, 'Cypress')
    # Kliknięcie na labelkę
    cypres_label.click()

    # Pobranie listy tytułów
    posts = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    # Asercja że lista ma 1 element
    assert len(posts) == 1

